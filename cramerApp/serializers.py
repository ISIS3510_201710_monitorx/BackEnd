from django.contrib.auth.models import User
from rest_framework import serializers
from .models import *


## App

class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course

class MeetingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Meeting