from django.apps import AppConfig

class CramerConfig(AppConfig):
    name = 'cramerApp'
