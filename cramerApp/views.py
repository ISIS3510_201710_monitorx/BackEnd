from django.shortcuts import render
from rest_framework import filters
from rest_framework import viewsets
from .serializers import *
from .models import *

def index(request):
    return render(request, 'index.html')

# App

class CourseViewSet(viewsets.ModelViewSet):
    #authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    #permission_classes = (IsAuthenticated,estaDeshabilitado)
    queryset = Course.objects.all().order_by('-id')
    serializer_class = CourseSerializer
    filter_backends = (filters.DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('price', 'name')
    ordering_fields = ('price', 'name')

class MeetingViewSet(viewsets.ModelViewSet):
    #authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    #permission_classes = (IsAuthenticated,estaDeshabilitado)
    queryset = Meeting.objects.all().order_by('-id')
    serializer_class = MeetingSerializer