from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from .views import *

router = DefaultRouter()
router.register(r'courses', CourseViewSet)
router.register(r'meetings', MeetingViewSet)


urlpatterns = [
    url(r'^', include(router.urls))
]
urlpatterns += router.urls
