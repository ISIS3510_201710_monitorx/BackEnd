from django.db import models
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

# App

class Course(models.Model):
    def __str__(self):
        return self.name + ' - id: '+ str(self.id)
    price = models.FloatField()
    image = models.CharField(max_length=500, unique=True)
    name = models.CharField(max_length=300, unique=True)
    description = models.TextField()

class Meeting(models.Model):
    user = models.CharField(max_length=300, unique=False)
    place = models.CharField(max_length=300, unique=False)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)

    # python manage.py makemigrations cramerApp
    # python manage.py sqlmigrate cramerApp 0001
    # python manage.py migrate
    # python manage.py createsuperuser
    # $ heroku run python manage.py migrate
    # git push heroku master